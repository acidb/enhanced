; Make file for Standard Websites

; Core version
; ------------

core = 7.x

; API version
; ------------

api = 2


; Modules
; --------

projects[breakpoints][version] = 1.3
projects[breakpoints][type] = "module"
projects[breakpoints][subdir] = "contrib"

projects[captcha][version] = 1.1
projects[captcha][type] = "module"
projects[captcha][subdir] = "contrib"

projects[ctools][version] = 1.4
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[colorbox][version] = 2.8
projects[colorbox][type] = "module"
projects[colorbox][subdir] = "contrib"

projects[context][version] = 3.2
projects[context][type] = "module"
projects[context][subdir] = "contrib"

projects[devel][version] = 1.5
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"

projects[features][version] = 2.2
projects[features][type] = "module"
projects[features][subdir] = "contrib"

projects[email][version] = 1.3
projects[email][type] = "module"
projects[email][subdir] = "contrib"

projects[extlink][version] = 1.18
projects[extlink][type] = "module"
projects[extlink][subdir] = "contrib"

projects[i18n][version] = 1.11
projects[i18n][type] = "module"
projects[i18n][subdir] = "contrib"

projects[inline_messages][version] = 1.0
projects[inline_messages][type] = "module"
projects[inline_messages][subdir] = "contrib"

projects[backup_migrate][version] = 3.0
projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "contrib"

projects[entity][version] = 1.5
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[entityreference][version] = 1.1
projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"

projects[field_group][version] = 1.4
projects[field_group][type] = "module"
projects[field_group][subdir] = "contrib"

projects[flexslider][version] = 2.0-alpha3
projects[flexslider][type] = "module"
projects[flexslider][subdir] = "contrib"

projects[globalredirect][version] = 1.5
projects[globalredirect][type] = "module"
projects[globalredirect][subdir] = "contrib"

projects[gmap][version] = 2.9
projects[gmap][type] = "module"
projects[gmap][subdir] = "contrib"

projects[imagecache_actions][version] = 1.4
projects[imagecache_actions][type] = "module"
projects[imagecache_actions][subdir] = "contrib"

projects[l10n_update][version] = 1.0
projects[l10n_update][type] = "module"
projects[l10n_update][subdir] = "contrib"

projects[less][version] = 3.0
projects[less][type] = "module"
projects[less][subdir] = "contrib"

projects[libraries][version] = 2.2
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

projects[linkit][version] = 3.1
projects[linkit][type] = "module"
projects[linkit][subdir] = "contrib"

projects[media][version] = 1.4
projects[media][type] = "module"
projects[media][subdir] = "contrib"

projects[menu_block][version] = 2.4
projects[menu_block][type] = "module"
projects[menu_block][subdir] = "contrib"

projects[metatag][version] = 1.1
projects[metatag][type] = "module"
projects[metatag][subdir] = "contrib"

projects[modernizr][version] = 3.3
projects[modernizr][type] = "module"
projects[modernizr][subdir] = "contrib"

projects[nodeblock][version] = 1.6
projects[nodeblock][type] = "module"
projects[nodeblock][subdir] = "contrib"

projects[omega_tools][version] = 3.0-rc4
projects[omega_tools][type] = "module"
projects[omega_tools][subdir] = "contrib"

projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"

projects[pathauto_persist][version] = 1.3
projects[pathauto_persist][type] = "module"
projects[pathauto_persist][subdir] = "contrib"

projects[rabbit_hole][version] = 2.23
projects[rabbit_hole][type] = "module"
projects[rabbit_hole][subdir] = "contrib"

projects[references][version] = 2.1
projects[references][type] = "module"
projects[references][subdir] = "contrib"

projects[scheduler][version] = 1.2
projects[scheduler][type] = "module"
projects[scheduler][subdir] = "contrib"

projects[search404][version] = 1.3
projects[search404][type] = "module"
projects[search404][subdir] = "contrib"

projects[simple_gmap][version] = 1.2
projects[simple_gmap][type] = "module"
projects[simple_gmap][subdir] = "contrib"

projects[spamspan][version] = 1.1-beta1
projects[spamspan][type] = "module"
projects[spamspan][subdir] = "contrib"

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[token][version] = 1.5
projects[token][type] = "module"
projects[token][subdir] = "contrib"

projects[transliteration][version] = 3.2
projects[transliteration][type] = "module"
projects[transliteration][subdir] = "contrib"

projects[ckeditor][version] = 1.15
projects[ckeditor][type] = "module"
projects[ckeditor][subdir] = "contrib"

projects[jquery_plugin][version] = 1.0
projects[jquery_plugin][type] = "module"
projects[jquery_plugin][subdir] = "contrib"

projects[jquery_update][version] = 2.4
projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"

projects[variable][version] = 2.5
projects[variable][type] = "module"
projects[variable][subdir] = "contrib"

projects[views][version] = 3.8
projects[views][type] = "module"
projects[views][subdir] = "contrib"

projects[view_unpublished][version] = 1.1
projects[view_unpublished][type] = "module"
projects[view_unpublished][subdir] = "contrib"

projects[webform][version] = 4.0
projects[webform][type] = "module"
projects[webform][subdir] = "contrib"

projects[wysiwyg][version] = 2.2
projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"

projects[xmlsitemap][version] = 2.0
projects[xmlsitemap][type] = "module"
projects[xmlsitemap][subdir] = "contrib"

projects[date][version] = 2.8
projects[date][type] = "module"
projects[date][subdir] = "contrib"

projects[picture][version] = 2.7
projects[picture][type] = "module"
projects[picture][subdir] = "contrib"

projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = 2.0-alpha2
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"

projects[views_column_class][version] = 1.0-alpha1
projects[views_column_class][type] = "module"
projects[views_column_class][subdir] = "contrib"


; Themes
; ------

projects[omega][version] = 3.1
projects[omega][type] = "theme"

; Theme
; -----

; change this to desired theme
projects[cloned_sheep][type] = "theme"
projects[cloned_sheep][download][type] = "git"
projects[cloned_sheep][download][destination] = "theme"
projects[cloned_sheep][download][url]= "https://acidb@bitbucket.org/acidb/cloned-sheep.git"


; Libraries
; ---------

; CKEditor 3.6.6
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.3/ckeditor_4.4.3_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

; LESS
libraries[less][download][type] = "git"
libraries[less][download][url] = "https://github.com/leafo/lessphp.git"
libraries[less][download][tag] = v0.4.0
libraries[less][directory_name] = "lessphp"
libraries[less][destination] = "libraries"

; FLEXSLIDER
libraries[flexslider][download][type] = "git"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider.git"
libraries[flexslider][download][tag] = version/2.2
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][destination] = "libraries"

; COLORBOX
libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox.git"
libraries[colorbox][download][tag] = 1.5.6
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][destination] = "libraries"

